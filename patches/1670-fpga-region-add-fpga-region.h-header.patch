From 998a488f5372a0ddde3baf9ca805ba2669fce0a0 Mon Sep 17 00:00:00 2001
From: Alan Tull <atull@kernel.org>
Date: Wed, 15 Nov 2017 14:20:21 -0600
Subject: [PATCH 1670/1795] fpga: region: add fpga-region.h header

* Create fpga-region.h.
* Export fpga_region_program_fpga.
* Move struct fpga_region and other things to the header.

This is a step in separating FPGA region common code
from Device Tree support.

Signed-off-by: Alan Tull <atull@kernel.org>
Acked-by: Moritz Fischer <mdf@kernel.org>
Signed-off-by: Greg Kroah-Hartman <gregkh@linuxfoundation.org>
(cherry picked from commit 59460a9305458ac3e7f2415b602dbaa6cfcb8a3b)
Signed-off-by: Geert Uytterhoeven <geert+renesas@glider.be>
---
 drivers/fpga/fpga-region.c       | 24 ++++--------------------
 include/linux/fpga/fpga-region.h | 28 ++++++++++++++++++++++++++++
 2 files changed, 32 insertions(+), 20 deletions(-)
 create mode 100644 include/linux/fpga/fpga-region.h

diff --git a/drivers/fpga/fpga-region.c b/drivers/fpga/fpga-region.c
index 435aab39c182..23527c1b2fa7 100644
--- a/drivers/fpga/fpga-region.c
+++ b/drivers/fpga/fpga-region.c
@@ -18,6 +18,7 @@
 
 #include <linux/fpga/fpga-bridge.h>
 #include <linux/fpga/fpga-mgr.h>
+#include <linux/fpga/fpga-region.h>
 #include <linux/idr.h>
 #include <linux/kernel.h>
 #include <linux/list.h>
@@ -26,24 +27,6 @@
 #include <linux/slab.h>
 #include <linux/spinlock.h>
 
-/**
- * struct fpga_region - FPGA Region structure
- * @dev: FPGA Region device
- * @mutex: enforces exclusive reference to region
- * @bridge_list: list of FPGA bridges specified in region
- * @mgr: FPGA manager
- * @info: fpga image specific information
- */
-struct fpga_region {
-	struct device dev;
-	struct mutex mutex; /* for exclusive reference to region */
-	struct list_head bridge_list;
-	struct fpga_manager *mgr;
-	struct fpga_image_info *info;
-};
-
-#define to_fpga_region(d) container_of(d, struct fpga_region, dev)
-
 static DEFINE_IDA(fpga_region_ida);
 static struct class *fpga_region_class;
 
@@ -233,7 +216,7 @@ static int fpga_region_get_bridges(struct fpga_region *region,
  * Program an FPGA using fpga image info (region->info).
  * Return 0 for success or negative error code.
  */
-static int fpga_region_program_fpga(struct fpga_region *region)
+int fpga_region_program_fpga(struct fpga_region *region)
 {
 	struct device *dev = &region->dev;
 	struct fpga_image_info *info = region->info;
@@ -289,6 +272,7 @@ static int fpga_region_program_fpga(struct fpga_region *region)
 
 	return ret;
 }
+EXPORT_SYMBOL_GPL(fpga_region_program_fpga);
 
 /**
  * child_regions_with_firmware
@@ -674,5 +658,5 @@ subsys_initcall(fpga_region_init);
 module_exit(fpga_region_exit);
 
 MODULE_DESCRIPTION("FPGA Region");
-MODULE_AUTHOR("Alan Tull <atull@opensource.altera.com>");
+MODULE_AUTHOR("Alan Tull <atull@kernel.org>");
 MODULE_LICENSE("GPL v2");
diff --git a/include/linux/fpga/fpga-region.h b/include/linux/fpga/fpga-region.h
new file mode 100644
index 000000000000..8a355171406b
--- /dev/null
+++ b/include/linux/fpga/fpga-region.h
@@ -0,0 +1,28 @@
+#ifndef _FPGA_REGION_H
+#define _FPGA_REGION_H
+
+#include <linux/device.h>
+#include <linux/fpga/fpga-mgr.h>
+#include <linux/fpga/fpga-bridge.h>
+
+/**
+ * struct fpga_region - FPGA Region structure
+ * @dev: FPGA Region device
+ * @mutex: enforces exclusive reference to region
+ * @bridge_list: list of FPGA bridges specified in region
+ * @mgr: FPGA manager
+ * @info: FPGA image info
+ */
+struct fpga_region {
+	struct device dev;
+	struct mutex mutex; /* for exclusive reference to region */
+	struct list_head bridge_list;
+	struct fpga_manager *mgr;
+	struct fpga_image_info *info;
+};
+
+#define to_fpga_region(d) container_of(d, struct fpga_region, dev)
+
+int fpga_region_program_fpga(struct fpga_region *region);
+
+#endif /* _FPGA_REGION_H */
-- 
2.19.0

