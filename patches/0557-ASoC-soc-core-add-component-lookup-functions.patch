From 87522cc7ac0dde898788a16e7c8f36aa4cfe8257 Mon Sep 17 00:00:00 2001
From: Kuninori Morimoto <kuninori.morimoto.gx@renesas.com>
Date: Mon, 2 Oct 2017 05:09:52 +0000
Subject: [PATCH 0557/1795] ASoC: soc-core: add component lookup functions

ALSA SoC platform/codec will be replaced to component soon.
This means 1 device might have multiple components. But current
unregister component function only checks "dev" to find it.
This means, unexpected component might be unregistered by current
function.
But, it is no problem if driver registered only 1 component.

To prepare avoid this issue, this patch adds new component
lookup function. it finds component by "dev" and "driver name".

Here, the reason why it uses "driver name" is that "component name"
was created by fmt_single_name() and difficult to use it from driver.
Driver of course knows its "driver name", thus, using it is more easy.

Signed-off-by: Kuninori Morimoto <kuninori.morimoto.gx@renesas.com>
Signed-off-by: Mark Brown <broonie@kernel.org>
(cherry picked from commit 7dd5d0d954611c05a38bdf843054c639f45ce08b)
Signed-off-by: Simon Horman <horms+renesas@verge.net.au>
Signed-off-by: Geert Uytterhoeven <geert+renesas@glider.be>
---
 include/sound/soc.h  |  2 ++
 sound/soc/soc-core.c | 26 ++++++++++++++++++++++++++
 2 files changed, 28 insertions(+)

diff --git a/include/sound/soc.h b/include/sound/soc.h
index d22de9712c45..c289e1b1ad84 100644
--- a/include/sound/soc.h
+++ b/include/sound/soc.h
@@ -475,6 +475,8 @@ int devm_snd_soc_register_component(struct device *dev,
 			 const struct snd_soc_component_driver *component_driver,
 			 struct snd_soc_dai_driver *dai_drv, int num_dai);
 void snd_soc_unregister_component(struct device *dev);
+struct snd_soc_component *snd_soc_lookup_component(struct device *dev,
+						   const char *driver_name);
 int snd_soc_cache_init(struct snd_soc_codec *codec);
 int snd_soc_cache_exit(struct snd_soc_codec *codec);
 
diff --git a/sound/soc/soc-core.c b/sound/soc/soc-core.c
index fee4b0ef5566..a9066320b672 100644
--- a/sound/soc/soc-core.c
+++ b/sound/soc/soc-core.c
@@ -3448,6 +3448,32 @@ void snd_soc_unregister_component(struct device *dev)
 }
 EXPORT_SYMBOL_GPL(snd_soc_unregister_component);
 
+struct snd_soc_component *snd_soc_lookup_component(struct device *dev,
+						   const char *driver_name)
+{
+	struct snd_soc_component *component;
+	struct snd_soc_component *ret;
+
+	ret = NULL;
+	mutex_lock(&client_mutex);
+	list_for_each_entry(component, &component_list, list) {
+		if (dev != component->dev)
+			continue;
+
+		if (driver_name &&
+		    (driver_name != component->driver->name) &&
+		    (strcmp(component->driver->name, driver_name) != 0))
+			continue;
+
+		ret = component;
+		break;
+	}
+	mutex_unlock(&client_mutex);
+
+	return ret;
+}
+EXPORT_SYMBOL_GPL(snd_soc_lookup_component);
+
 static int snd_soc_platform_drv_probe(struct snd_soc_component *component)
 {
 	struct snd_soc_platform *platform = snd_soc_component_to_platform(component);
-- 
2.19.0

