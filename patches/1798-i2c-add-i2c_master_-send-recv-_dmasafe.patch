From e25f98864b047a55098b572bc606c5908cb0d3ef Mon Sep 17 00:00:00 2001
From: Wolfram Sang <wsa+renesas@sang-engineering.com>
Date: Sat, 4 Nov 2017 21:20:05 +0100
Subject: [PATCH 1798/1808] i2c: add i2c_master_{send|recv}_dmasafe

Use the new helper to create variants of i2c_master_{send|recv} which
mark their buffers as DMA safe.

Signed-off-by: Wolfram Sang <wsa+renesas@sang-engineering.com>
Acked-by: Jonathan Cameron <Jonathan.Cameron@huawei.com>
Signed-off-by: Wolfram Sang <wsa@the-dreams.de>
(cherry picked from commit ba98645c7d54640f163096cda3609d4d55c6ae54)
Signed-off-by: Simon Horman <horms+renesas@verge.net.au>
Signed-off-by: Geert Uytterhoeven <geert+renesas@glider.be>
---
 include/linux/i2c.h | 32 ++++++++++++++++++++++++++++++++
 1 file changed, 32 insertions(+)

diff --git a/include/linux/i2c.h b/include/linux/i2c.h
index 375392c3c2c0..a099c3af7721 100644
--- a/include/linux/i2c.h
+++ b/include/linux/i2c.h
@@ -80,6 +80,22 @@ static inline int i2c_master_recv(const struct i2c_client *client,
 	return i2c_transfer_buffer_flags(client, buf, count, I2C_M_RD);
 };
 
+/**
+ * i2c_master_recv_dmasafe - issue a single I2C message in master receive mode
+ *			     using a DMA safe buffer
+ * @client: Handle to slave device
+ * @buf: Where to store data read from slave, must be safe to use with DMA
+ * @count: How many bytes to read, must be less than 64k since msg.len is u16
+ *
+ * Returns negative errno, or else the number of bytes read.
+ */
+static inline int i2c_master_recv_dmasafe(const struct i2c_client *client,
+					  char *buf, int count)
+{
+	return i2c_transfer_buffer_flags(client, buf, count,
+					 I2C_M_RD | I2C_M_DMA_SAFE);
+};
+
 /**
  * i2c_master_send - issue a single I2C message in master transmit mode
  * @client: Handle to slave device
@@ -94,6 +110,22 @@ static inline int i2c_master_send(const struct i2c_client *client,
 	return i2c_transfer_buffer_flags(client, (char *)buf, count, 0);
 };
 
+/**
+ * i2c_master_send_dmasafe - issue a single I2C message in master transmit mode
+ *			     using a DMA safe buffer
+ * @client: Handle to slave device
+ * @buf: Data that will be written to the slave, must be safe to use with DMA
+ * @count: How many bytes to write, must be less than 64k since msg.len is u16
+ *
+ * Returns negative errno, or else the number of bytes written.
+ */
+static inline int i2c_master_send_dmasafe(const struct i2c_client *client,
+					  const char *buf, int count)
+{
+	return i2c_transfer_buffer_flags(client, (char *)buf, count,
+					 I2C_M_DMA_SAFE);
+};
+
 /* Transfer num messages.
  */
 extern int i2c_transfer(struct i2c_adapter *adap, struct i2c_msg *msgs,
-- 
2.17.1

