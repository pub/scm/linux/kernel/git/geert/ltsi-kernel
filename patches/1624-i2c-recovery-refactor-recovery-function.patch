From 637c5e7113abc07e2f1eeeb405abf04b451e9320 Mon Sep 17 00:00:00 2001
From: Wolfram Sang <wsa+renesas@sang-engineering.com>
Date: Tue, 10 Jul 2018 23:42:17 +0200
Subject: [PATCH 1624/1795] i2c: recovery: refactor recovery function

After exiting the while loop, we checked if recovery was successful and
sent a STOP to the clients. Meanwhile however, we send a STOP after
every pulse, so it is not needed after the loop. If we move the check
for a free bus to the end of the while loop, we can shorten and simplify
the logic. It is still ensured that at least one STOP will be sent to
the wire even if SDA was not stuck low.

Signed-off-by: Wolfram Sang <wsa+renesas@sang-engineering.com>
Reviewed-by: Peter Rosin <peda@axentia.se>
Signed-off-by: Wolfram Sang <wsa@the-dreams.de>
(cherry picked from commit 0b71026c69caa10261218528326721828d29a481)
Signed-off-by: Simon Horman <horms+renesas@verge.net.au>
Signed-off-by: Geert Uytterhoeven <geert+renesas@glider.be>
---
 drivers/i2c/i2c-core-base.c | 24 ++++++------------------
 1 file changed, 6 insertions(+), 18 deletions(-)

diff --git a/drivers/i2c/i2c-core-base.c b/drivers/i2c/i2c-core-base.c
index ea4cc8defada..7f768c99464f 100644
--- a/drivers/i2c/i2c-core-base.c
+++ b/drivers/i2c/i2c-core-base.c
@@ -225,9 +225,6 @@ static int i2c_generic_recovery(struct i2c_adapter *adap)
 				ret = -EBUSY;
 				break;
 			}
-			/* Break if SDA is high */
-			if (bri->get_sda && bri->get_sda(adap))
-				break;
 		}
 
 		val = !val;
@@ -243,22 +240,13 @@ static int i2c_generic_recovery(struct i2c_adapter *adap)
 		if (bri->set_sda)
 			bri->set_sda(adap, val);
 		ndelay(RECOVERY_NDELAY / 2);
-	}
-
-	/* check if recovery actually succeeded */
-	if (bri->get_sda && !bri->get_sda(adap))
-		ret = -EBUSY;
 
-	/* If all went well, send STOP for a sane bus state. */
-	if (ret == 0 && bri->set_sda) {
-		bri->set_scl(adap, 0);
-		ndelay(RECOVERY_NDELAY / 2);
-		bri->set_sda(adap, 0);
-		ndelay(RECOVERY_NDELAY / 2);
-		bri->set_scl(adap, 1);
-		ndelay(RECOVERY_NDELAY / 2);
-		bri->set_sda(adap, 1);
-		ndelay(RECOVERY_NDELAY / 2);
+		/* Break if SDA is high */
+		if (val && bri->get_sda) {
+			ret = bri->get_sda(adap) ? 0 : -EBUSY;
+			if (ret == 0)
+				break;
+		}
 	}
 
 	if (bri->unprepare_recovery)
-- 
2.19.0

