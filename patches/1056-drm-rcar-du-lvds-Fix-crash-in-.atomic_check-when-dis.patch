From 0b2f5ec90c76e79b098dfc367a8a96b2bb54e54f Mon Sep 17 00:00:00 2001
From: Laurent Pinchart <laurent.pinchart+renesas@ideasonboard.com>
Date: Fri, 27 Apr 2018 22:40:21 +0300
Subject: [PATCH 1056/1795] drm: rcar-du: lvds: Fix crash in .atomic_check when
 disabling connector

The connector .atomic_check() handler can be called with a NULL crtc
pointer in the connector state when the connector gets disabled
explicitly (through performing a legacy mode set or setting the
connector's CRTC_ID property to 0). This causes a crash as the crtc
pointer is dereferenced without any check.

Fix it by returning from the .atomic_check() handler when then crtc
pointer is NULL, as there is no check to be performed when the connector
gets disabled.

Fixes: c6a27fa41fab ("drm: rcar-du: Convert LVDS encoder code to bridge driver")
Signed-off-by: Laurent Pinchart <laurent.pinchart+renesas@ideasonboard.com>
Reviewed-by: Kieran Bingham <kieran.bingham+renesas@ideasonboard.com>
(cherry picked from commit 643ca198aacc671f32ef7c0c2783f0b539070a36)
Signed-off-by: Simon Horman <horms+renesas@verge.net.au>
Signed-off-by: Geert Uytterhoeven <geert+renesas@glider.be>
---
 drivers/gpu/drm/rcar-du/rcar_lvds.c | 3 +++
 1 file changed, 3 insertions(+)

diff --git a/drivers/gpu/drm/rcar-du/rcar_lvds.c b/drivers/gpu/drm/rcar-du/rcar_lvds.c
index 3d2d3bbd1342..155ad840f3c5 100644
--- a/drivers/gpu/drm/rcar-du/rcar_lvds.c
+++ b/drivers/gpu/drm/rcar-du/rcar_lvds.c
@@ -88,6 +88,9 @@ static int rcar_lvds_connector_atomic_check(struct drm_connector *connector,
 	const struct drm_display_mode *panel_mode;
 	struct drm_crtc_state *crtc_state;
 
+	if (!state->crtc)
+		return 0;
+
 	if (list_empty(&connector->modes)) {
 		dev_dbg(lvds->dev, "connector: empty modes list\n");
 		return -EINVAL;
-- 
2.19.0

